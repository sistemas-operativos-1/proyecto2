var express=require('express');
var app = express();
var MongoClient =require('mongodb').MongoClient;
var url='mongodb://35.225.245.55:27017/';
var str = "";
var cors = require('cors')
app.use(cors());

var redis = require('redis');
var client = redis.createClient({host:'34.72.157.182', port:'6379'} );
client.on('connect', function() {
    console.log('connected');
});



app.get("/ultimo", (req, res) => {
	client.lindex('mylist',-1,(err, result) => {
		if(err){
			return res.status(500).send(err);
		}
		console.log(result);
        res.json(result);
        return;
	})
});



//Todos los departamentos-Mongo
app.route('/Departamentos').get(function(req, res)
{
MongoClient.connect(url, {useUnifiedTopology: true},function(err, db) {
    console.log("connected");
    if (err) throw err;
        var dbo = db.db("proyecto");
        var cursor=dbo.collection("casos").aggregate([
            {
                $group : {
                    _id : '$location',
                    count : {$sum : 1}
                }
            }
        ]).toArray(function(err, docs) {
            console.log(docs);
            res.json(docs);
            return;
          });
});
});

//Top 3 -Mongo
app.route('/Top').get(function(req, res)
{
MongoClient.connect(url, {useUnifiedTopology: true},function(err, db) {
    console.log("connected");
    if (err) throw err;
        var dbo = db.db("proyecto");
        var cursor=dbo.collection("casos").aggregate([
            {
                $group : {
                    _id : '$location',
                    count : {$sum : 1}
                }
            },
            {$sort: {'count':-1} },
            {$limit:3}
        ]).toArray(function(err, docs) {
            console.log(docs);
            res.json(docs);
            return;
          });
});
});


//edades
app.route('/Edades').get(function(req, res)
{
MongoClient.connect(url, {useUnifiedTopology: true},function(err, db) {
    console.log("connected");
    if (err) throw err;
        var dbo = db.db("proyecto");
        var cursor=dbo.collection("casos").find(
            {
                age : {
                    $gt: 0,
                    $lte : 100
                }
            }
        ).toArray(function(err, docs) {
            //console.log(docs);
            res.json(docs);
            console.log(docs.length);
            return;
          });
});
});

var server = app.listen(3002, function() {});


