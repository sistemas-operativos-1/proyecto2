import './App.css';
import React,{ Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {HorizontalBar, Pie} from 'react-chartjs-2';


class App extends Component {
  state={
    respuesta:[],
    respuestadepartamentos:[],
    respuestaTop:[],
    departamentoTop:[],
    noCasos:[],
    colores:[],
    data:[],
    opciones:[],
    dataBar:[],
    opcionesBar:[],
    age:[],
    infectedtype:[],
    location:[],
    name:[],
    state:[],
    departamentos:[],
    nodepartamentos:[],
    edad1:[],
    edad2:[],
    edad3:[],
    edad4:[],
    edad5:[],
    edad6:[],
    edad7:[],
    edad8:[],
    edad9:[],
    edad10:[]

  }

  async  Top(){
    let peticion=await fetch('http://35.223.111.58:3002/Top');
    let respuesta =await peticion.json();
   this.setState({ respuestaTop: respuesta});
   var d=[], c=[];
   for(var i=0;i<respuesta.length;i++)
   {
    d.push(respuesta[i]._id);
    c.push(respuesta[i].count);
   }
   this.setState({departamentoTop:d,noCasos:c});
   console.log("departamentosTop",this.state.departamentoTop);
   console.log("Casos",this.state.noCasos);
  }
  async  UltimoCasopeticion(){
    var peticion=await fetch('http://35.223.111.58:3002/ultimo');
    var respuesta =await peticion.json();
    var ultimocaso=respuesta.split(/[:,{}]+/);
    console.log(ultimocaso);
    this.setState({age:ultimocaso[6],infectedtype:ultimocaso[8], location:ultimocaso[4], name:ultimocaso[2], state:ultimocaso[10]});
   
  

}
  async  EdadesPeticion(){
    let peticion=await fetch('http://35.223.111.58:3002/Edades');
    let respuesta =await peticion.json();
   console.log("edades",respuesta);

   for(var i=0; i<respuesta.length;i++)
   {
     let edad=respuesta[i].age;
     if(edad<=10){
      this.state.edad1.push(edad);
      console.log("edad1",this.state.edad1);
     }else if(edad>=11 & edad<=20)
      {
        this.state.edad2.push(edad);
        console.log("edad2",this.state.edad2);
      
      }else if(edad>=21 & edad<=30)
      {
        this.state.edad3.push(edad);
        console.log("edad3",this.state.edad3);
     
      }else if(edad>=31 & edad<=40)
      {
        this.state.edad4.push(edad);
        console.log("edad4",this.state.edad4);
      }else if(edad>=41 & edad<=50)
      {
        this.state.edad5.push(edad);
        console.log("edad5",this.state.edad5);
      }else if(edad>=51 & edad<=60)
      {
        this.state.edad6.push(edad);
        console.log("edad6",this.state.edad6);
      }else if(edad>=61 & edad<=70)
      {
        this.state.edad7.push(edad);
        console.log("edad7",this.state.edad7);
      }else if(edad>=71 & edad<=80)
      {
        this.state.edad8.push(edad);
        console.log("edad8",this.state.edad8);
      }else if(edad>=81 & edad<=90)
      {
        this.state.edad9.push(edad);
        console.log("edad9",this.state.edad9);
      }else if(edad>=91 & edad<=100)
      {
        this.state.edad10.push(edad);
        console.log("edad10",this.state.edad10);
      }
    
   
   
  }
}
  async  TodosDepartamentos(){
    let peticion=await fetch('http://35.223.111.58:3002/Departamentos');
    let respuesta =await peticion.json();
   this.setState({ respuesta: respuesta});
   var d=[], c=[];
   for(var i=0;i<respuesta.length;i++)
   {
    d.push(respuesta[i]._id);
    c.push(respuesta[i].count);
   }
   this.setState({departamentos:d,nodepartamentos:c});
   console.log("departamentos",this.state.departamentos);
   console.log("no",this.state.nodepartamentos);
  }


  generarCaracter(){
    var caracter=["a","b","c","d","e","f","0","1","2","3","4","5","6","7","8","9"];
    var numero=(Math.random()*15).toFixed(0);
    return caracter[numero];
  }

  colorHEX(){
    var color="";
    for(var i=0;i<6;i++){
      color=color+this.generarCaracter();
    }
    return "#"+color;
  }

  generarColores(){
    var colores=[];
    for(var i=0; i<this.state.respuesta.length;i++){
      colores.push(this.colorHEX());
    }
    this.setState({colores: colores});
    console.log("colores",this.state.colores);
  }
  configurarGrafica(){
    const data={
      labels: this.state.departamentos,
      datasets:[{
        data: this.state.nodepartamentos,
        backgroundColor: this.state.colores
      }]
    };
    const opciones={
      responsive: true,
      maintainAspectRatio: false
    }
    this.setState({data:data, opciones:opciones});
  }

  configurarGraficaBarras()
  {     
    var barChart={
      type: 'horizontalBar',
        labels: ['0-10','11-20','21-30','31-40','41-50','51-60','61-70','71-80','81-90','91-100'],
        datasets: [{
          data:  [this.state.edad1.length,this.state.edad2.length,this.state.edad3.length,this.state.edad4.length,this.state.edad5.length,this.state.edad6.length,this.state.edad7.length,this.state.edad8.length,this.state.edad9.length,this.state.edad10.length],
          backgroundColor: 'orange',
          label: ['edades'],
        }]
      };
      const opcionesb={
        scales: {
          yAxes: [{
            barPercentage: 0.5
          }]
        },
        elements: {
          rectangle: {
            borderSkipped: 'left',
          }
        },
        responsive: true,
      maintainAspectRatio: false
      }
    console.log("barchat",barChart);
    console.log("opcionesBar",opcionesb);
    this.setState({dataBar:barChart, opcionesBar:opcionesb});
}



  async componentDidMount(){
    await this.UltimoCasopeticion();
    await this.EdadesPeticion();
    await this.Top();
    await this.TodosDepartamentos();
    await this.generarColores();
    this.generarColores();
    this.configurarGrafica();
    this.configurarGraficaBarras();
  }
  render (){
    return(
    <div className="App">
      <header className="App-header">
        <h1>Coronavirus</h1>
        <br></br>
        <h3>Top tres de departamentos con más casos</h3>
        <table className="table table-dark">
        <thead>
          <tr>
            <th>No</th>
            <th>Departamento</th>
            <th>No. casos </th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td>1</td>
          <td>{this.state.departamentoTop[0]}</td>
          <td>{this.state.noCasos[0]}</td>
        </tr>
        <tr>
          <td>2</td>
          <td>{this.state.departamentoTop[1]}</td>
          <td>{this.state.noCasos[1]}</td>
        </tr>
        <tr>
          <td>3</td>
          <td>{this.state.departamentoTop[2]}</td>
          <td>{this.state.noCasos[2]}</td>
        </tr>
      </tbody>
      </table>
      <br></br>
      <h3>Departamentos afectados</h3>
      <Pie data={this.state.data} opciones={this.state.opciones}/>
      <br></br>
      <h3>Ultimo caso agregado</h3>
      <table className="table table-dark">
        <thead>
          <tr>
            <th>Name</th>
            <th>Location</th>
            <th>Age</th>
            <th>infectedtype</th>
            <th>State</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td>{this.state.name}</td>
          <td>{this.state.location}</td>
          <td>{this.state.age}</td>
          <td>{this.state.infectedtype}</td>
          <td>{this.state.state}</td>
        </tr>
      </tbody>
      </table>
      <br></br>
      <h3>Cantidad de afectados por rango de 10 años</h3>
      <HorizontalBar data={this.state.dataBar} opciones={this.state.opcionesBar}/>
      </header>
      
    </div>
    )
  }
}

export default App;
